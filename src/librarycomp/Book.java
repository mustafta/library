/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package librarycomp;

/**
 *
 * @author Taghreed
 */
public class Book {
    public String title;
    public String author;
    
    public Book (String title, String author)
    {
    this.title=title;
    this.author=author;
    
    }
    
    public String geTitle()
    {
    
    return title;
    }
     public String getAuthor()
    {
    
    return author;
    }
    
}
