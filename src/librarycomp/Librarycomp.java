/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package librarycomp;
import java.util.ArrayList;
/**
 *
 * @author Taghreed
 */
public class Librarycomp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Book book1 = new Book("Introduction to Java", "Liang");
       Book book2 = new Book("Java, The Complete Reference " , " H. Schildt");
       Book book3 = new Book("Core Java" , "C. Horstmann");
       
       ArrayList<Book> books = new ArrayList<Book>();
       books.add(book1);
       books.add(book2);
       books.add(book3);
       
       Library lib = new Library(books);
       ArrayList<Book> bks = lib.getTotalBook();
       
       for (Book bk: bks){
       System.out.println("Title :" + bk.title + " and "+ "Author " + bk.author);
       }
       
       
    }
    
}
